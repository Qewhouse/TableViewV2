//
//  DetailedViewController.swift
//  TableViewV2
//
//  Created by Alexander Altman on 13.09.2023.
//

import UIKit

class DetailedViewController: UIViewController {
    let color: UIColor
    let colorName: String
    
    init(color: UIColor, colorName: String) {
        self.color = color
        self.colorName = colorName
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create and configure UI elements in the DetailedViewController
        view.backgroundColor = .white
        
        let coloredSquare = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        coloredSquare.backgroundColor = color
        coloredSquare.center = view.center
        
        let label = UILabel(frame: CGRect(x: 0, y: 120, width: 200, height: 30))
        label.text = colorName
        label.textAlignment = .center
        
        view.addSubview(coloredSquare)
        view.addSubview(label)
    }
}
