//
//  TableViewSectionModel.swift
//  TableViewV2
//
//  Created by Alexander Altman on 13.09.2023.
//

import UIKit

// Define a TableViewSectionModel to hold section data, including header and cells
class TableViewSectionModel {
    var cells: [TableViewCellModel]
    var header: TableViewHeaderModel?
    
    init(cells: [TableViewCellModel]) {
        self.cells = cells
    }
}
