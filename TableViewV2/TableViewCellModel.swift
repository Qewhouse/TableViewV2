//
//  TableViewCellModel.swift
//  TableViewV2
//
//  Created by Alexander Altman on 13.09.2023.
//

import UIKit

// Define a TableViewCellModel to hold cell data and behavior
class TableViewCellModel {
    let identifier: String
    var cellHeight: CGFloat = 44.0
    var onFill: ((UITableViewCell) -> Void)?
    var onSelect: (() -> Void)?
    var color: UIColor
    var colorName: String
    
    init(identifier: String, color: UIColor, colorName: String) {
        self.identifier = identifier
        self.color = color
        self.colorName = colorName
    }
    
}
