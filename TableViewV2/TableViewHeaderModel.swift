//
//  TableViewHeaderModel.swift
//  TableViewV2
//
//  Created by Alexander Altman on 13.09.2023.
//

import UIKit

// Define a TableViewHeaderModel to hold header data and behavior
class TableViewHeaderModel {
    // You can add header properties and behavior here if needed
    // For example, you can have a title or any other customization.
}
