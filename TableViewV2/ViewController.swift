//
//  ViewController.swift
//  TableViewV2
//
//  Created by Alexander Altman on 13.09.2023.
//

import UIKit

class ViewController: UIViewController {
     var tableView: UITableView!
     var tableViewBuilder: TableViewBuilder!
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .cyan
        
        // Create the UITableView programmatically
        tableView = UITableView(frame: view.bounds, style: .plain)
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(tableView)
        
        // Register your custom cell class
        tableView.register(CustomCell.self, forCellReuseIdentifier: "customCellIdentifier")
        
        
        // Initialize TableViewBuilder with the tableView
        tableViewBuilder = TableViewBuilder(tableView: tableView)
        
        // Create cell model
        let cellModels: [TableViewCellModel] = [
            TableViewCellModel(identifier: "customCellIdentifier", color: .random(), colorName: "Red"),
            TableViewCellModel(identifier: "customCellIdentifier", color: .random(), colorName: "Blue"),
            TableViewCellModel(identifier: "customCellIdentifier", color: .random(), colorName: "Green"),
            // Add more cell models as needed
        ]
        
        // Configure the onFill and onSelect methods for each cell model
                for cellModel in cellModels {
                    
                    cellModel.onFill = { cell in
                        if let customCell = cell as? CustomCell {
                            customCell.configure(withColor: cellModel.color, colorName: cellModel.colorName)
                        }
                    }
                    
                    cellModel.onSelect = { [weak self] in
                        // Handle cell selection, e.g., navigate to another screen
                        print("tap")
                        self?.navigateToDetailViewController(withColor: cellModel.color, colorName: cellModel.colorName)
                    }
                }
        
        
        
        // Create section model with the cell
        let sectionModel = TableViewSectionModel(cells: cellModels)
        
        // Optionally, set a header for the section
        sectionModel.header = TableViewHeaderModel()
        
        // Set sections to the TableViewBuilder
        tableViewBuilder.sections = [sectionModel]
    }
    
    func navigateToDetailViewController(withColor color: UIColor, colorName: String) {
        let detailViewController = DetailedViewController(color: color, colorName: colorName)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}

