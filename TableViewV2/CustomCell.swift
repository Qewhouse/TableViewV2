//
//  CustomCell.swift
//  TableViewV2
//
//  Created by Alexander Altman on 13.09.2023.
//

import UIKit

class CustomCell: UITableViewCell {
    // Add any UI elements you want in your custom cell here
    // For example, you can add a colored square view and a label
    
    let coloredSquareView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let colorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // Customize the cell's appearance here
        selectionStyle = .none
        
        // Add subviews and layout constraints for your UI elements
        contentView.addSubview(coloredSquareView)
        contentView.addSubview(colorLabel)
        
        NSLayoutConstraint.activate([
            coloredSquareView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            coloredSquareView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            coloredSquareView.widthAnchor.constraint(equalToConstant: 30),
            coloredSquareView.heightAnchor.constraint(equalToConstant: 30),
            
            colorLabel.leadingAnchor.constraint(equalTo: coloredSquareView.trailingAnchor, constant: 16),
            colorLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(withColor color: UIColor, colorName: String) {
        // Customize cell content based on the data passed
        coloredSquareView.backgroundColor = color
        colorLabel.text = colorName
    }
}

